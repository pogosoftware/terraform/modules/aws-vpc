variable "azs" {
  description = "The Availbility zones in which should be create subnets."
  type        = list(string)
}

variable "cidr_block" {
  description = "The cidr block of the desired VPC."
  type        = string
}

variable "create_nat_gateway" {
  default     = true
  description = "Determinates if create nat gateways for private routes or not"
  type        = bool
}

variable "enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"
  type        = bool
  default     = true
}

variable "enable_dns_support" {
  description = "Whether or not the VPC has DNS support."
  type        = bool
  default     = true
}

variable "instance_tenancy" {
  description = "The allowed tenancy of instances launched into the selected VPC. May be any of \"default\", \"dedicated\", or \"host\"."
  type        = string
  default     = "default"
}

variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
}

variable "networks" {
  description = "The networks."
  type = list(object({
    id          = string
    new_bits    = number
    route_table = string
  }))
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
