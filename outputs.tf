##########################################################################
### VPC
##########################################################################

output "vpc_id" {
  description = "The ID of the VPC."
  value       = aws_vpc.main.id
}

output "vpc_arn" {
  description = "Amazon Resource Name (ARN) of VPC."
  value       = aws_vpc.main.arn
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC."
  value       = aws_vpc.main.cidr_block
}

output "vpc_instance_tenancy" {
  description = "Tenancy of instances spin up within VPC."
  value       = aws_vpc.main.instance_tenancy
}

output "vpc_enable_dns_support" {
  description = "Whether or not the VPC has DNS support."
  value       = aws_vpc.main.enable_dns_support
}

output "vpc_enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support."
  value       = aws_vpc.main.enable_dns_hostnames
}

##########################################################################
### SUBNET
##########################################################################

output "subnet_ids" {
  description = "The subnet ids."
  value       = local.subnet_ids
}

output "subnet_ids_by_id" {
  description = "The subnets ids for specific network."
  value       = local.subnet_ids_by_id
}

output "subnect_cidr_blocks" {
  description = "The subnets cidr blocks"
  value       = module.subnets.network_cidr_blocks
}

##########################################################################
### INTERNET GATEWAY
##########################################################################

output "igw_id" {
  description = "The ID of the Internet Gateway."
  value       = aws_internet_gateway.main[0].id
}

output "igw_arn" {
  description = "The ARN of the Internet Gateway."
  value       = aws_internet_gateway.main[0].arn
}

##########################################################################
### NAT GATEWAY
##########################################################################

output "nat_gw_ids" {
  description = "The NAT Gateway ids."
  value       = [for nat in aws_nat_gateway.main : nat.id]
}

output "nat_gw_public_ips" {
  description = "The NAT Gateway public ips."
  value       = [for nat in aws_nat_gateway.main : nat.public_ip]
}

output "nat_gw_private_ips" {
  description = "The NAT Gateway private ips."
  value       = [for nat in aws_nat_gateway.main : nat.private_ip]
}

##########################################################################
### ROUTE TABLE
##########################################################################

output "rt_id_by_name" {
  description = "The route table ids by route table name."
  value       = local.rt_id_by_name
}
