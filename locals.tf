locals {
  region = data.aws_region.current.name

  networks = flatten([
    for az in var.azs : [
      for net in var.networks : {
        id          = net.id
        name        = format("%s_%s", net.id, substr(az, length(az) - 1, 1))
        new_bits    = net.new_bits
        route_table = net.route_table
      }
    ]
  ])

  public_subnets_count = length({ for net in local.networks : net.name => net.name if net.route_table == "public" })
  route_tables         = distinct([for net in local.networks : net.route_table])

  nat_eips     = var.create_nat_gateway == false ? {} : { for net in local.networks : net.name => net.name if net.id == "public" }
  nat_gateways = var.create_nat_gateway == false ? {} : { for net in local.networks : net.name => net.name if net.id == "public" }
  nat_gateways_routes = var.create_nat_gateway == false ? [] : flatten([
    for rt in local.route_tables : [
      for ng in local.nat_gateways : {
        route_table_id = rt
        nat_gateway_id = ng
      }
    ] if rt == "private"
  ])

  ###########################################################################
  ### OUTPUTS
  ###########################################################################
  network_ids = [for net in var.networks : net.id]
  subnet_ids  = { for k, v in module.subnets.network_cidr_blocks : k => aws_subnet.main[k].id }
  subnet_ids_by_id = {
    for n in local.network_ids : n => [
      for az in var.azs : local.subnet_ids[format("%s_%s", n, substr(az, length(az) - 1, 1))]
    ]
  }
  rt_id_by_name = { for rt in local.route_tables : rt => aws_route_table.main[rt].id }
}
